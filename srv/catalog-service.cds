using { timoschuetz.demo.bookshop as my } from '../db/schema';

service CatalogService @(path:'/browse') {
    @readonly entity Books as select from my.Books;
}