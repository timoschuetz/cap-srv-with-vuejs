using { timoschuetz.demo.bookshop as my } from '../db/schema';

// Better way to expose data
// Query information: http://localhost:4004/explore/Authors?$expand=books($select=ID,title)

service ExploreService @(path:'/explore') {
    
    @readonly entity Books as select from my.Books {
       key ID, title, author
    };

    @readonly entity Authors as select from my.Authors {
        key ID, name, books
    };

    // @requires: 'authenticated-user'
    @insertonly entity Orders as projection on my.Orders;
    action cancelOrder ( ID:Orders.ID, reason: String );
}