using { timoschuetz.demo.bookshop as my } from '../db/schema';

service AdminService @(_requires: 'admin') {
    entity Books as projection on my.Books excluding {
        createdBy, createdAt, modifiedBy, modifiedAt
    };
    entity Authors as projection on my.Authors excluding {
        createdBy, createdAt, modifiedBy, modifiedAt
    };
    entity Orders as select from my.Orders;
}